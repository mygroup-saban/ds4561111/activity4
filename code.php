<?php

class Building {
    private $floors;
    private $address;
    protected $name;
    
    public function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    
    public function getFloors() {
        return $this->floors;
    }
    
    public function setFloors($floors) {
        $this->floors = $floors;
    }
    
    public function getAddress() {
        return $this->address;
    }
    
    public function setAddress($address) {
        $this->address = $address;
    }
    
    public function getName() {
        return $this->name;
    } 
    
    public function setName($name) {
        $this->name = $name;

    }

}

class Condominium extends Building {

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getFloors() {
        return parent::getFloors();
    } 
    // public function setFloors($floors) {
    //     $this->floors = $floors;
    // }

    public function getAddress() {
        return parent::getAddress();
    }

    // public function setAddress($address) {
    //     $this->address = $address;
    // }
}

$building = new Building('Caswynn Building', 8, 'Timog Ave., Quezon City', 'Philippines');

$condominium = new Condominium('Enzo Condominium', 5, 'Buendia Ave., Makati City', 'Philippines');